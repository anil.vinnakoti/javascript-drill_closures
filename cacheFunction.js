function cacheFunction(callback){
    const cache = { };

    return function invokesCallbackFn(...args) {
            const uniqueIdentifierForArgs = [...args];
            if(cache.hasOwnProperty(uniqueIdentifierForArgs)){
                return cache[uniqueIdentifierForArgs];
            }
            else{
                cache[uniqueIdentifierForArgs] = callback(...args);
                return cache[uniqueIdentifierForArgs];
            }
    }
};


module.exports = cacheFunction;