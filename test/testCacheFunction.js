const cacheFunction = require('../cacheFunction');


function addingThreeNumbers(a,b,c){
    return a+b+c;
};

const result = cacheFunction(addingThreeNumbers);

console.log(result(1,2,3));
console.log(result(1,2,3));


console.log(result(2,1,3));
console.log(result(2,1,3));