const counterFactory = require('../counterFactory');

const counter = counterFactory();

let increment = counter.increment(5);
console.log(`The counter value 5 is increased to ${increment}`);

const decrement = counter.decrement(5);
console.log(`The counter value 5 is decreased to ${decrement}`);