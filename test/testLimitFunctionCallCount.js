const limitFunctionCallCount = require('../limitFunctionCallCount');

function printHello(){
    console.log('Hello')
    return;
}

const callbackFnResult = limitFunctionCallCount(printHello, 5);
console.log(callbackFnResult());