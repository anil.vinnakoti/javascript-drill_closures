function limitFunctionCallCount(callback, number){

    return function functionToInvokeCallback(){
        if(number > 0 && number != 0){
            while(number > 0){
                callback();
                number--;
             }
             if(number == 0){
             return null;
             }
        }
        else {
            return ('Invalid input. Please provide number value greater than 0');
        }
            
     };
};


module.exports = limitFunctionCallCount;